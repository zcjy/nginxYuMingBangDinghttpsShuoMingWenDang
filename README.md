# nginx域名绑定https说明文档 #
简单安装配置https证书说明  

## 1.获取证书 ##
首先登陆到腾讯云\阿里云的后台证书管理选项下找到购买的证书列表如下:  
 
![图片截图](https://gitee.com/uploads/images/2017/1226/120417_23d6bc76_1414088.png "C1F~VF3QC(SPN_`DPQ~472S.png")

点击下载会得到一个对应域名的文件夹解压缩后查看到对应文件夹:  
  
![图片截图](https://gitee.com/uploads/images/2017/1226/120732_82d33adf_1414088.png "X_}XS~(BD5_%]B17U`S1V0K.png")  
  
`Nginx`文件夹内获得SSL证书文件 `1_www.domain.com_bundle.crt` 和私钥文件 `2_www.domain.com.key`,
1_www.domain.com_bundle.crt 文件包括两段证书代码 “-----BEGIN CERTIFICATE-----”和“-----END CERTIFICATE-----”,
2_www.domain.com.key 文件包括一段私钥代码“-----BEGIN RSA PRIVATE KEY-----”和“-----END RSA PRIVATE KEY-----”。

## 2.证书安装 ##
将域名 www.domain.com 的证书文件1_www.domain.com_bundle.crt 、私钥文件2_www.domain.com.key保存到`同一个目录`，例如`/etc/nginx/`目录下。更新Nginx根目录下 `nginx.conf`或者在conf.d目录下新建*.conf文件如下：  

     server{
		listen 80;
		#listen [::]:80 ipv6only=on default_server;
		server_name bybm.yunlike.cn;
		
		
		set $root_path '/var/www/weisite/bybm/public';
		root $root_path;
		
		index index.php;

		location / {
			try_files $uri $uri/ /index.php?$query_string;
		}
		#try_files $uri $uri/ @rewrite;

		#location @rewrite {
		#		rewrite ^/(.*)$ /index.php?_url=/$1;
		#}

		location ~ \.php {
				fastcgi_pass 127.0.0.1:9000;
				fastcgi_index /index.php;

				include /etc/nginx/fastcgi_params;

				fastcgi_split_path_info       ^(.+\.php)(/.+)$;
				fastcgi_param PATH_INFO       $fastcgi_path_info;
				fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
				fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		}

		location ~* ^/(css|img|js|flv|swf|download)/(.+)$ {
				root $root_path;
		}

		location ~ /\.ht {
				deny all;
		}
	}

	server{
		listen 443;
		#listen [::]:80 ipv6only=on default_server;
		server_name 			bybm.yunlike.cn;
		ssl on;
		ssl_certificate 		/etc/nginx/1_bybm.yunlike.cn_bundle.crt; #特别注意这里需要是绝对路径最佳
		ssl_certificate_key 	/etc/nginx/2_bybm.yunlike.cn.key; #特别注意这里需要是绝对路径最佳
		ssl_session_timeout 	5m;
		ssl_protocols TLSv1 TLSv1.1 TLSv1.2; #按照这个协议配置
		ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:HIGH:!aNULL:!MD5:!RC4:!DHE;#按照这个套件配置
		ssl_prefer_server_ciphers on;
		
		set $root_path '/var/www/weisite/bybm/public';
		root $root_path;
		
		index index.php;

		location / {
			try_files $uri $uri/ /index.php?$query_string;
		}
		#try_files $uri $uri/ @rewrite;

		#location @rewrite {
		#		rewrite ^/(.*)$ /index.php?_url=/$1;
		#}

		location ~ \.php {
				fastcgi_pass 127.0.0.1:9000;
				fastcgi_index /index.php;

				include /etc/nginx/fastcgi_params;

				fastcgi_split_path_info       ^(.+\.php)(/.+)$;
				fastcgi_param PATH_INFO       $fastcgi_path_info;
				fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
				fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		}

		location ~* ^/(css|img|js|flv|swf|download)/(.+)$ {
				root $root_path;
		}

		location ~ /\.ht {
				deny all;
		}
	}

    include /etc/nginx/conf.d/*.conf;
	
	#设置允许发布内容为8M
	client_max_body_size 8M;
	client_body_buffer_size 464k;  

配置完成后，先用`bin/nginx –t`来测试下配置是否有误，正确无误的话，重启nginx。就可以使 https://www.domain.com 来访问了。

## 3.注意安全组规则添加443端口 ##
上面配置结束后如无法正常访问,很大原因是443的端口没有开放,需要登录到阿里云\腾讯云后台中添加443端口的安全组规则。  

![输入图片说明](https://gitee.com/uploads/images/2017/1226/131406_90d48771_1414088.png "SKK~)E)Y0]ODKYD~58%3FI2.png")


